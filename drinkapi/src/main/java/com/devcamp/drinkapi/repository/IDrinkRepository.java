package com.devcamp.drinkapi.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinkapi.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
    CDrink findById(long id);
    
}
