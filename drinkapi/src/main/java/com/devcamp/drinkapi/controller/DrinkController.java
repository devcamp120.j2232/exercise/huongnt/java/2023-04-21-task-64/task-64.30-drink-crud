package com.devcamp.drinkapi.controller;

import java.util.Date;
//import java.util.ArrayList;
import java.util.List;
//import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinkapi.model.CDrink;
import com.devcamp.drinkapi.repository.IDrinkRepository;
//import com.devcamp.drinkapi.repository.IDrinkRepository;
import com.devcamp.drinkapi.service.DrinkService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {

	@Autowired
	DrinkService pDrinkService;
	
    //GET tất cả dữ liệu dùng service
  @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            return new ResponseEntity<>(pDrinkService.getDrinkList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }	

	   // Lấy voucher theo {id} dùng service
	   @GetMapping("/drinks/{id}")// Dùng phương thức GET
	   public ResponseEntity<CDrink> getCDrinkById(@PathVariable("id") long id) {
		try {
            CDrink drinkData = pDrinkService.getDrinkById(id);
            return new ResponseEntity<>(drinkData,HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
	   }
     @Autowired IDrinkRepository pIDrinkRepository;
    //Tạo MỚI drink KHÔNG dùng service sử dụng phương thức POST, phải truyền ID trong postman nếu k thì bị lỗi
    /* 
       @Autowired IDrinkRepository pIDrinkRepository;
       @PostMapping("/drinks")
       public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pDrinks) {
           try {
               //TODO: Hãy viết code tạo voucher đưa lên DB
               Optional<CDrink> drinkData = pIDrinkRepository.findById(pDrinks.getId());
               if(drinkData.isPresent()) {
                   return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
               }
               pDrinks.setNgayTao(new Date());
               pDrinks.setNgayCapNhat(null);
               CDrink _vouchers = pIDrinkRepository.save(pDrinks);
               return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);			
           } catch (Exception e) {
               System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
               //Hiện thông báo lỗi tra back-end
               //return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
               //return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
               return ResponseEntity.unprocessableEntity().body("Failed to Create specified Drink: "+e.getCause().getCause().getMessage());
           }
       }*/


       //cách này k cần truyền ID trong postman
       @PostMapping("/drinks")
       public ResponseEntity<Object> createDrink1(@Valid @RequestBody CDrink pDrinks) {
         try {
           pDrinks.setNgayTao(new Date());
           pDrinks.setNgayCapNhat(null);
           CDrink _drinks = pIDrinkRepository.save(pDrinks);
           return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
         } catch (Exception e) {
           System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
           return ResponseEntity.unprocessableEntity()
               .body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
         }
       }
     
   
  // cập nhật drinks
  @PutMapping("/drinks/{id}")
  public ResponseEntity<Object> updateDrink(@PathVariable("id") int id, @Valid @RequestBody CDrink pDrinks) {
    try {
      CDrink drinkData = pIDrinkRepository.findById(id);
      CDrink drink = drinkData;
      drink.setMaNuocUong(pDrinks.getMaNuocUong());
      drink.setTenNuocUong(pDrinks.getTenNuocUong());
      drink.setDonGia(pDrinks.getDonGia());
      drink.setGhiChu(pDrinks.getGhiChu());
      drink.setNgayCapNhat(new Date());
      try {
        return new ResponseEntity<>(pIDrinkRepository.save(drink), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Drink: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // xóa theo id
  @DeleteMapping("/drinks/{id}")
  public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") int id) {
    try {
      pIDrinkRepository.deleteById((long) id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // xóa tất cả
  @DeleteMapping("/drinks")
  public ResponseEntity<CDrink> deleteAllDrink() {
    try {
      pIDrinkRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

