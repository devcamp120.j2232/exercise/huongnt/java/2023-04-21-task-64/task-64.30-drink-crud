package com.devcamp.drinkapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.drinkapi.model.CDrink;
import com.devcamp.drinkapi.repository.IDrinkRepository;

@Service
public class DrinkService {
    @Autowired
    IDrinkRepository pDrinkRepository;
    public ArrayList<CDrink> getDrinkList() {
        ArrayList<CDrink> drinkList = new ArrayList<>();
        pDrinkRepository.findAll().forEach(drinkList::add);
        return drinkList;
    }


    public CDrink getDrinkById(long id){
        CDrink drinkListById = pDrinkRepository.findById(id).get();
        return drinkListById;
    }
}
    
