-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2023-04-21 16:14:24
-- サーバのバージョン： 10.4.27-MariaDB
-- PHP のバージョン: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `pizza365_db`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `ma_nuoc_uong` varchar(255) DEFAULT NULL,
  `ngay_cap_nhat` bigint(20) DEFAULT NULL,
  `ngay_tao` bigint(20) DEFAULT NULL,
  `gia_nuoc_uong` bigint(20) DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- テーブルのデータのダンプ `drinks`
--

INSERT INTO `drinks` (`id`, `drink_code`, `ngay_cap_nhat`, `ngay_tao`, `price`, `drink_name`) VALUES
(1, 'MT', 20230415, 20230415, 15000, 'Milk tea'),
(2, 'KT', 20230415, 20230415, 10000, 'Kumquat tea'),
(3, 'COKE', 20230415, 20230415, 15000, 'Cocacola'),
(4, 'PEP', 20230415, 20230415, 12000, 'Pepsi'),
(5, 'WTER', 20230415, 20230415, 10000, 'Water'),
(6, 'OT', 20230415, 20230415, 13000, 'Olong tea');
--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ioautl9bhwm9nmdgersk8mjx` (`ma_nuoc_uong`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
